import Data.Char
import Data.List

data Op     = Plus | Minus | Mul | Div
data Expr   = Number Integer | Subexpr Expr Op Expr
data Token  = Op | Expr
data Tree a = Empty | Node (Tree a) a (Tree a)

instance Show Op where
    show Minus = "-"
    show Plus  = "+"
    show Mul   = "*"
    show Div   = "/"

instance (Show a) => Show (Tree a) where
--instance Show (Tree) where
    show (Empty) = ""
    show (Node Empty x Empty) =
        show x
    show (Node left x right) =
        "(" ++ show left ++ ")" ++ (show x) ++ "(" ++ show right ++ ")"

instance Show Expr where
    show (Number i) = show i
    show (Subexpr left op right) = 
        "(" ++
        show left ++
        show op ++
        show right ++
        ")"

leaf x = Node Empty x Empty

priority Plus = 1
priority Minus = 1
priority Mul = 2
priority Div = 2

isNumber' str = isNum str where
    isNum [] = True
    isNum (h:t) = isDigit h && isNum t

findMinOpIndex [] = -1
findMinOpIndex lst =
    case (findIndices (\x -> x `elem` ["+", "-"]) lst) of
        [] -> case (findIndices (\x -> x `elem` ["*","/"]) lst) of
            [] -> -1
            l -> last l
        l -> last l

exprToTree expr = exprToTree' $ words expr where
    exprToTree' [] = Empty
    exprToTree' [x] = leaf x
    exprToTree' list =
        Node (exprToTree' left) x (exprToTree' right) where
            i = findMinOpIndex list
            x = list !! i
            left = take i list
            right = drop (i + 1) list


treeToRPN Empty = ""
treeToRPN (Node Empty x Empty) = x ++ " "
treeToRPN (Node left x right) =
    treeToRPN left ++ treeToRPN right ++ x ++ " "

solveRPN = head . foldl foldingFunction [] . words
    where foldingFunction (x:y:ys) "*" = (x * y):ys
          foldingFunction (x:y:ys) "+" = (x + y):ys
          foldingFunction (x:y:ys) "-" = (y - x):ys
          foldingFunction (x:y:ys) "/" = (y `div` x):ys
          foldingFunction xs numberString = read numberString:xs 

--calculateFromRPN "" = 0
--calculateFromRPN str =
--    calc str "" where
--        calc (h:t) = 
