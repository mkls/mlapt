--nCr :: Integer -> Integer -> Either String Integer
nCr m n
  | m < 0 || n < 0 = Left "Arguments should be positive"
  | n - m < 0      = Left "Subset size shouldn't be greater than set size"
  | otherwise      =
      Right $ div (fac n 1) (fac m 1 * fac (n - m) 1) where
          fac x acc
            | x == 0 = acc
            | otherwise = fac (x - 1) (acc * x)

--euler :: Integral a => a -> Either String a
euler x
  | x < 1 = Left "Euler function is defined for natural numbers only"
  | otherwise =
      Right $ euler' x 1 where
          euler' 1 _ = 1
          euler' t g
            | g > t = 0
            | gcd t g == 1 = 1 + euler' t (g + 1)
            | otherwise = euler' t (g + 1)
