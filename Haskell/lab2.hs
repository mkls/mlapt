import Data.Either (fromRight)

findNearest [] = Left "Minimum is undefined for empty list"
findNearest lst = Right $ filter (\(a,b,c) -> c == min' lst) lst where
    min' lst = minimum $ (\(a,b,c) -> c) $ unzip3 lst

eliminateFifths [] = Left "Cannot reduce empty list"
eliminateFifths [a] = Right a
eliminateFifths participants =
    Right $ f 1 participants [] where
        f _ [a] [] = a
        f c [] acc = f c acc []
        f c (h:t) acc
          | c `mod` 5 == 0 = f (c + 1) t acc
          | otherwise = f (c + 1) t $ acc ++ [h]

mostHolidays :: (Ord b, Num b) => [(a, b)] -> Maybe b
mostHolidays [] = Nothing
mostHolidays dates =
    Just $ snd $ max' months where
        max' [a] = a
        max' (h:t) = max'' h $ max' t

        max'' (a,b) (c,d)
          | a > c = (a,b)
          | otherwise = (c,d)

        months = occurances 1 $ map snd dates
        occurances x l
          | x > 12 = []
          | otherwise =
              (length $ filter (== x) l, x): occurances (x+1) l

queens :: [(Integer, Integer)] -> Bool
queens [] = False
queens (h:t) =
    check h t where
        check _ [] = False
        check x (h:t) = queens' x (h:t) || check h t

        queens' _ [] = False
        queens' x [y] = beats x y
        queens' x (h:t)
          | beats x h = True
          | otherwise = queens' x t
        beats x y
          | fst x == fst y || snd x == snd y || dx == dy = True
          | otherwise = False where
              dx = abs $ fst x - fst y
              dy = abs $ snd x - snd y

selfDual [] = False
selfDual [a, b] = a /= b
selfDual (f:t)
  | f /= last t = selfDual $ withoutLast t
  | otherwise = False where
      withoutLast [a] = []
      withoutLast (h:t) = h : withoutLast t
