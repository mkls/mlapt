#include <iostream>
#include <fstream>
#include <iomanip>
#include "../hxx/packageReader.hxx"

using namespace std;

int main() {
    ofstream out("output", ios::out);
    ifstream in("data_some_error.bin", ios::binary);

    out << setw(11) << "Ax"
        << setw(11) << "Ay"
        << setw(11) << "Az"
        << setw(11) << "Wx"
        << setw(11) << "Wy"
        << setw(11) << "Wz"
        << setw(12) << "Tax"
        << setw(12) << "Tay"
        << setw(12) << "Taz"
        << setw(12) << "Twx"
        << setw(12) << "Twy"
        << setw(12) << "Twz"
        << setw(12) << "S"
        << setw(12) << "Timestamp"
        << setw(8) << "Status"
        << setw(8) << "Number\n";

    PackageReader reader(out);

    uint8_t tmp;
    while (in >> noskipws >> tmp) {
        reader.readByte(tmp);
    }

    reader.showResults(cout);

    out.close();
    in.close();

    return 0;
}
