#include "../hxx/packageReader.hxx"
#include "../hxx/util.hxx"
#include <iomanip>

void PackageReader::dump() {
    // ID=0x87 always have length = 45 bytes
    // 0: ID
    // 1-4 bytes: Ax, int32_t
    // 5-8: Ay, int32_t
    // 9-12: Az, int32_t
    // 13-16: Wx, int32_t
    // 17-20: Wy, int32_t
    // 21-24: Wz, int32_t
    // 25-26: Tax, int16_t
    // 27-28: Tay, int16_t
    // 29-30: Taz, int16_t
    // 31-32: Twx, int16_t
    // 33-34: Txy, int16_t
    // 35-36: Txz, int16_t
    // 37-38: S, int16_t
    // 39-40: Timestamp, int16_t
    // 41: Status, uint8_t
    // 42: Number, uint8_t
    // 43-44: CRC, uint16_t

    int32_t t32 = 0;
    int16_t t16 = 0;
    int i = 1;

    for (int k = 1; k <= 6; k++) {
        t32 = 0;
        for (int z = 1; z <= 4; z++, i++) {
            t32 |= (int32_t) currentPackage[i] << 8 * (z - 1);
        }
        stream << noskipws << setw(11) << t32;
    }

    for (int k = 0; k < 8; k++) {
        t16 = 0;
        for (size_t z = 1; z <= 2; z++, i++) {
            t16 |= (int16_t)currentPackage[i] << 8 * (z - 1);
        }
        stream << noskipws << setw(12) << t16;
    }

    // status
    stream << noskipws << setw(8) << (int)currentPackage[41];
    // number
    stream << noskipws << setw(7) << (int)currentPackage[42] << "\n";
}

void PackageReader::showResults(ostream &o) {
    cout << "Good/bad/total: "
         << this->goodPackages << "/"
         << this->brokenPackages << "/"
         << this->goodPackages + this->brokenPackages << "\n";
}

PackageReader::PackageReader(ostream &o) : stream(o) {
    packageSize = 0;
    lastByteI = 0;
    state = SYNC;
    currentPackage = nullptr;
    goodPackages = 0;
    brokenPackages = 0;
}

void PackageReader::readByte(uint8_t byte) {
    switch (state) {
        case SYNC:
            sync(byte);
            break;
        case LEN:
            readLength(byte);
            break;
        case DATA:
            readData(byte);
    }
}

void PackageReader::sync(uint8_t byte) {
    if (byte == 0xAA && ++lastByteI == 2) {
        state = LEN;
        lastByteI = 0;
    } else if (byte != 0xAA){
        lastByteI = 0;
    }
}

void PackageReader::readLength(uint8_t byte) {
    packageSize = byte;
    currentPackage = new uint8_t[packageSize];
    state = DATA;
}

void PackageReader::readData(uint8_t byte) {
    currentPackage[lastByteI++] = byte;

    if (lastByteI == packageSize) {
        checkPackage();
    }
}

void PackageReader::checkPackage() {
    if (currentPackage[0] == 0x87) {
        uint16_t checkSum = crc16(currentPackage, (int)packageSize - 2);
        uint16_t pkgSum = 256u * currentPackage[packageSize - 1] + currentPackage[packageSize - 2];

        if (checkSum == pkgSum) {
            goodPackages++;
            dump();
        } else {
            brokenPackages++;
        }
    }

    delete[] this->currentPackage;
    this->packageSize = 0;
    this->lastByteI = 0;

    state = SYNC;
}
