#ifndef AUTO_PACKAGEREADER_HXX
#define AUTO_PACKAGEREADER_HXX

#include <cstdint>
#include <ostream>

using namespace std;

class PackageReader {
private:
    enum State {SYNC, LEN, DATA, CHECK} state;

    uint8_t *currentPackage;
    size_t lastByteI;
    long long packageSize;

    size_t goodPackages;
    size_t brokenPackages;

    ostream &stream;

    void dump();
    void sync(uint8_t);
    void readLength(uint8_t);
    void readData(uint8_t);
    void checkPackage();
public:
    PackageReader(ostream &);
    void readByte(uint8_t);
    void showResults(ostream &);
};


#endif //AUTO_PACKAGEREADER_HXX
