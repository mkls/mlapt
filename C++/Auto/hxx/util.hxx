#ifndef AUTO_UTIL_HXX
#define AUTO_UTIL_HXX

#include <cstdint>

uint16_t crc16(const uint8_t *buf, int len);

#endif //AUTO_UTIL_HXX
