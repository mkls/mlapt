#ifndef POT_HPP
#define POT_HPP

#include "./cookware.hpp"

class Pot : virtual public Cookware {
private:
    float volume;
protected:
    bool callParent;
public:
    void print(void) override;
    std::ostream& print(std::ostream &os) override;
    Pot(ull, float);
};

#endif
