#ifndef GAS_RANGE_HPP
#define GAS_RANGE_HPP

#include "./cooker.hpp"
#include <string>

class GasRange : public Cooker {
protected:
    float consumption;
public:
    GasRange(ull, std::string, float);
    std::ostream& print(std::ostream &os) override;
    void print(void) override;
};

#endif
