#ifndef ECOOCKER_HPP
#define ECOOCKER_HPP

#include "./cooker.hpp"
#include <iostream>

class ElectricCooker : public Cooker {
private:
    ull wattage;
public:
    void print(void) override;
    std::ostream& print(std::ostream &os) override;
    ElectricCooker(ull, std::string, ull);
};

#endif
