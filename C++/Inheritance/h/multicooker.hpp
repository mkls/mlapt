#ifndef MULTICOOKER_HPP
#define MULTICOOKER_HPP

#include "./pot.hpp"
#include "./electricCooker.hpp"

class Multicooker : public ElectricCooker,
                    public Pot {
protected:
    bool hasPressureCooking;
public:
    void print(void) override;
    std::ostream& print(std::ostream &os) override;
    Multicooker(ull, std::string, ull, float, bool);
};

#endif
