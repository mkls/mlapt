#ifndef COOKER_HPP
#define COOKER_HPP

#include "./cookware.hpp"
#include <string>

class Cooker : virtual public Cookware {
private:
    std::string colour;
protected:
    bool callParent;
public:
    Cooker(ull, std::string);
    void print(void) override;
    std::ostream& print(std::ostream&) override;
};

#endif
