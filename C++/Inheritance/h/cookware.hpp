#ifndef COOKWARE_HPP
#define COOKWARE_HPP

#include <iostream>

#define ull unsigned long long

class Cookware {
protected:
    int inventoryId;

public:
    Cookware(ull);
    virtual ~Cookware(void) = default;
    virtual void print(void) = 0;
    virtual std::ostream& print(std::ostream&);

    bool operator <(const Cookware&);
    bool operator >(const Cookware&);
    bool operator <=(const Cookware&);
    bool operator >=(const Cookware&);
    bool operator ==(const Cookware&);
    bool operator !=(const Cookware&);


    friend std::ostream& operator <<(std::ostream&, Cookware&);
    explicit operator unsigned long long();
    explicit operator int();
};

#endif
