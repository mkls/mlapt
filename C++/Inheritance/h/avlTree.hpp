#ifndef AVL_TREE_HPP
#define AVL_TREE_HPP

// TODO: use smart pointers
//#include <memory>
//using std::unique_ptr;

template<class T, typename U>
class AvlTree {
private:
    struct Node {
        T val;
        Node* left;
        Node* right;
        long long height;

        static void rebalance(Node*&);
        static void simpleRight(Node*&);
        static void simpleLeft(Node*&);
        static void deleteNode(Node*&, Node*&);
        static bool insert(Node*&, T);

        Node(T val) {
            this->val = val;
            this->left = nullptr;
            this->right = nullptr;
        };
    };

    Node* root;

    static bool deleteNode(Node*&, U);
    static void traverseInOrder(Node *node);
    static void traversePreOrder(Node *node);
    static void traversePostOrder(Node *node);
    static long long getHeight(Node *);
public:
    AvlTree();
    ~AvlTree();

    bool insert(T);
    bool remove(U);

    void traverseInOrder(void);
    void traversePreOrder(void);
    void traversePostOrder(void);
};

#endif
