cmake_minimum_required(VERSION 3.17)
project(Inheritance)

set(CMAKE_CXX_STANDARD 14)

include_directories(h)

add_executable(Inheritance
        h/avlTree.hpp
        h/cooker.hpp
        h/cookware.hpp
        h/electricCooker.hpp
        h/gasRange.hpp
        h/multicooker.hpp
        h/pot.hpp
        src/avlTree.cpp
        src/cooker.cpp
        src/cookware.cpp
        src/electricCooker.cpp
        src/gasRange.cpp
        src/main.cpp
        src/multicooker.cpp
        src/pot.cpp)
