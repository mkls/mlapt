#include <iostream>
#include <exception>

#include "../h/cookware.hpp"
#include "../h/cooker.hpp"
#include "../h/electricCooker.hpp"
#include "../h/gasRange.hpp"
#include "../h/multicooker.hpp"
#include "../h/pot.hpp"
#include "../h/avlTree.hpp"

int main() {
    //AvlTree<std::unique_ptr<Cookware>, ull> tree;
    AvlTree<Cookware*, int> tree;
    std::string input;

    do {
        std::cout << "[i]nsert, [r]emove, [p]rint, [q]uit:\n> ";
        std::cin >> input;

        if (input == "i") {
            int id;
            std::string colour;
            float volume;
            float gasConsumption;
            int wattage;
            bool hasPressureCooking;

            std::string type;
            std::cout << "pot, cooker, gasRange, electricCooker, multicooker\n> ";
            std::cin >> type;

            std::cout << "id ";
            if (type == "pot") {
                std::cout << "volume\n> ";
                std::cin >> id  >> volume;

                tree.insert(new Pot(id, volume));
            } else if (type == "cooker") {
                std::cout << "colour\n> ";
                std::cin >> id >> colour;

                tree.insert(new Cooker(id, colour));
            } else if (type == "gasRange") {
                std::cout << "colour gasConsumption\n> ";
                std::cin >> id >> colour >> gasConsumption;

                tree.insert(new GasRange(id, colour, gasConsumption));
            } else if (type == "electricCooker") {
                std::cout << "colour wattage\n> ";
                std::cin >> id >> colour >> wattage;

                tree.insert(new ElectricCooker(id, colour, wattage));
            } else if (type == "multicooker") {
                std::cout << "colour wattage volume hasPressureCooking\n> ";
                std::cin >> id >> colour >> wattage >> volume >> hasPressureCooking;

                tree.insert(new Multicooker(id, colour, wattage, volume, hasPressureCooking));
            } else {
                input = "";
                std::cout << "Invalid input\n";
            }
        } else if (input == "r") {

            int id;
            std::cout << "id to remove\n> ";
            std::cin >> input;
            try {
                id = std::stoi(input);
            } catch (std::exception &e) {
                std::cout << "Invalid input\n";
                continue;
            }

            tree.remove(id);
        } else if (input == "p") {
            std::cout << "[1] In-order, [2] Pre-order, [3] Post-order\n> ";
            std::cin >> input;

            if (input == "1") {
                tree.traverseInOrder();
            } else if (input == "2") {
                tree.traversePreOrder();
            } else if (input == "3") {
                tree.traversePostOrder();
            } else {
                std::cout << "Invalid input\n";
            }
        }
    } while (input != "q" && !std::cin.eof());

    return 0;
}
