#include "../h/cooker.hpp"
#include <iostream>

void Cooker::print(void) {
    if (callParent)
        Cookware::print();
    std::cout << "Cooker colour: "<< this->colour << "\n";
}

std::ostream& Cooker::print(std::ostream &os) {
    if (callParent)
        Cookware::print(os);
    os << "Cooker colour: "<< this->colour << "\n";
    return os;
}

Cooker::Cooker(ull inventoryId, std::string colour) : Cookware(inventoryId) {
    this->colour = colour;
    this->callParent = true;
}
