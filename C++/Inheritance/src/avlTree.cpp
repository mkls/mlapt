#include "../h/avlTree.hpp"
#include "../h/cookware.hpp"
#include "../h/cooker.hpp"
#include "../h/electricCooker.hpp"
#include "../h/pot.hpp"
#include "../h/gasRange.hpp"
#include "../h/multicooker.hpp"

#include <cmath>
#include <iostream>

void printType(Cookware *t) {
    if (dynamic_cast<Multicooker*>(t)) {
        std::cout << "Multicooker:";
    } else if (dynamic_cast<ElectricCooker*>(t)) {
        std::cout << "Electric Cooker:";
    } else if (dynamic_cast<Pot*>(t)) {
        std::cout << "Pot:";
    } else if (dynamic_cast<GasRange*>(t)) {
        std::cout << "Gas range:";
    } else if (dynamic_cast<Cooker*>(t)) {
        std::cout << "Cooker:";
    } else {
        std::cout << "Undefined Cookware";
    }
    std::cout << "\n-------------------\n";
}

template<class T, typename U>
AvlTree<T, U>::AvlTree() {
    this->root = nullptr;
}

template<class T, typename U>
AvlTree<T, U>::~AvlTree() {
    if (root) {
        delete root->left;
        delete root->right;
    }

    delete root;
}

template<class T, typename U>
bool AvlTree<T, U>::insert(T item) {
    bool inserted = Node::insert(this->root, item);
    return inserted;
}

template<class T, typename U>
bool AvlTree<T, U>::Node::insert(Node*& node, T item) {
    bool inserted = false;
    if (!node) {
        node = new Node(item);
        inserted = true;
    } else if (*item < *(node->val)) {
        inserted = Node::insert(node->left, item);
    } else if (*(node->val) == *item) {
        std::cout << "Item with such id already exists.\n"
                  << *(node->val) << "\n";
        delete item;
        inserted = false;
    } else {
        inserted = Node::insert(node->right, item);
    }

    if (inserted) {
        node->height = getHeight(node);

        Node::rebalance(node);
    }

    return inserted;
}

template<class T, typename U>
void AvlTree<T, U>::Node::rebalance(Node*& node) {
    if (!node)
        return;
    long long leftHeight = getHeight(node->left);
    long long rightHeight = getHeight(node->right);

    long long balanceFactor = leftHeight - rightHeight;

    if (balanceFactor == -2) {
        long long rl = node->right ? getHeight(node->right->left) : 0;
        long long rr = node->right ? getHeight(node->right->right): 0;

        if (rl - rr == 1) {
            Node::simpleRight(node->right);
        }
        Node::simpleLeft(node);
    } else if (balanceFactor == 2) {
        long long ll = node->left ? getHeight(node->left->left) : 0;
        long long lr = node->left ? getHeight(node->left->right): 0;

        if (ll - lr == -1) {
            Node::simpleLeft(node->left);
        }
        Node::simpleRight(node);
    }
}

template<class T, typename U>
void AvlTree<T, U>::Node::simpleLeft(Node*& node) {
    auto tmp = node->right;
    node->right = tmp->left;
    tmp->left = node;
    node = tmp;
    tmp->height = fmaxl(getHeight(tmp->left), getHeight(tmp->right)) + 1;
    node->height = fmaxl(getHeight(node->left), getHeight(node->right)) + 1 ;
}

template<class T, typename U>
void AvlTree<T, U>::Node::simpleRight(Node*& node) {
    auto tmp = node->left;
    node->left = tmp->right;
    tmp->right = node;
    node = tmp;
    tmp->height = fmaxl(getHeight(tmp->left), getHeight(tmp->right)) + 1;
    node->height = fmaxl(getHeight(node->left), getHeight(node->right)) + 1 ;
}

template<class T, typename U>
bool AvlTree<T, U>::remove(U key) {
    bool status = deleteNode(root, key);
    Node::rebalance(root);
    return status;
}

template<class T, typename U>
bool AvlTree<T, U>::deleteNode(Node*& node, U key) {
    bool deleted = false;

    if (!node) {
        std::cout << "Item with id = " << key << " not found.\n";
        return false;
    }

    node->height = getHeight(node);

    if (key < (U)*(node->val)) {
        deleted = deleteNode(node->left, key);
    } else if (key > (U)*(node->val)) {
        deleted = deleteNode(node->right, key);
    } else {
        Node* q = node;
        if (q->right == nullptr) {
            node = node->left;
        } else if (q->left == nullptr) {
            node = q->right;
        } else {
            Node::deleteNode(q->left, q);
        }
        delete q;

        return true;
    }

    if (deleted) {
        node->height = getHeight(node);
        Node::rebalance(node);
        node->height = getHeight(node);
    }

    return true;
}

template<class T, typename U>
void AvlTree<T, U>::Node::deleteNode(Node*& r, Node*& q) {
    if (r->right != nullptr) {
        Node::deleteNode(r->right, q);
    } else {
        q->val = r->val;
        q = r;
        r = r->left;
        if (q)
            q->height = getHeight(q);
        if (r)
            r->height = getHeight(r);
       rebalance(q);
       rebalance(r);
    }
}

template<class T, typename U>
long long AvlTree<T, U>::getHeight(Node *node) {
    if (node == nullptr) {
        return 0;
    }

    return fmaxl(getHeight(node->left), getHeight(node->right)) + 1;
}

template<class T, typename U>
void AvlTree<T, U>::traverseInOrder(void) {
    traverseInOrder(root);
}

template<class T, typename U>
void AvlTree<T, U>::traversePreOrder(void) {
    traversePreOrder(root);
}

template<class T, typename U>
void AvlTree<T, U>::traversePostOrder(void) {
    if (!root)
        return;

    traversePostOrder(root);
}
template<class T, typename U>
void AvlTree<T, U>::traverseInOrder(Node* node) {
    if (!node) {
        return;
    }
    traverseInOrder(node->left);
    printType((node->val));
    std::cout << *(node->val) << "\n";
    traverseInOrder(node->right);
}


template<class T, typename U>
void AvlTree<T, U>::traversePreOrder(Node* node) {
    if (!node) {
        return;
    }
    printType((node->val));
    std::cout << *(node->val) << "\n";
    traversePreOrder(node->left);
    traversePreOrder(node->right);
}

template<class T, typename U>
void AvlTree<T, U>::traversePostOrder(Node* node) {
    if (!node) {
        return;
    }
    traversePostOrder(node->left);
    traversePostOrder(node->right);
    printType((node->val));
    std::cout << *(node->val) << "\n";
}

template class AvlTree<Cookware*, ull>;
template class AvlTree<Cookware*, int>;
