#include "../h/gasRange.hpp"
#include <iostream>

GasRange::GasRange(ull inventoryId,
                   std::string colour,
                   float consumption) : Cookware(inventoryId),
                                        Cooker(inventoryId, colour) {
    this->consumption = consumption;
}

std::ostream& GasRange::print(std::ostream &os) {
    Cooker::print(os);
    os << "Gas consumption: " << this->consumption << "\n";
    return os;
}

void GasRange::print(void) {
    Cooker::print();
    std::cout << "Gas consumption: " << this->consumption << "\n";
}
