#include "../h/multicooker.hpp"
#include <iostream>

Multicooker::Multicooker(ull inventoryId,
                         std::string colour,
                         ull wattage,
                         float volume,
                         bool hasPressureCooking) :
    Cookware(inventoryId),
    ElectricCooker(inventoryId, colour, wattage),
    Pot(inventoryId, volume) {

    this->hasPressureCooking = hasPressureCooking;
}

void Multicooker::print(void) {
    ElectricCooker::print();
    Pot::callParent = false;
    Pot::print();

    std::string answer = this->hasPressureCooking ?
                         "yes" :
                         "no";
    std::cout << "Pressure cooking" << answer << "\n";
}

std::ostream& Multicooker::print(std::ostream &os) {
    ElectricCooker::print(os);
    Pot::callParent = false;
    Pot::print(os);

    std::string answer = this->hasPressureCooking ?
                         "yes" :
                         "no";
    os << "Pressure cooking: " << answer << "\n";
    return os;
}
