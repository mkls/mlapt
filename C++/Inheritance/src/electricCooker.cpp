#include "../h/electricCooker.hpp"
#include <iostream>

ElectricCooker::ElectricCooker(ull inventoryId,
                               std::string colour,
                               ull wattage) : Cookware(inventoryId),
                                              Cooker(inventoryId, colour) {
    this->wattage = wattage;
}

void ElectricCooker::print(void) {
    Cooker::print();
    std::cout << "Wattage: " << this->wattage << "\n";
}

std::ostream& ElectricCooker::print(std::ostream &os) {
    Cooker::print(os);
    os << "Wattage: "<< this->wattage << "\n";
    return os;
}
