#include "../h/pot.hpp"
#include <iostream>

Pot::Pot(ull inventoryId, float volume) : Cookware(inventoryId) {
    this->callParent = true;
    this->volume = volume;
}

void Pot::print(void) {
    if (callParent)
        Cookware::print();
    std::cout << "Pot volume: " << this-> volume << "\n";
}

std::ostream& Pot::print(std::ostream &os) {
    if (callParent)
        Cookware::print(os);
    os << "Pot volume: " << this->volume << "\n";
    return os;
}
