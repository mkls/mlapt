#include "../h/cookware.hpp"
#include <iostream>

Cookware::Cookware(ull inventoryId) {
    this->inventoryId = inventoryId;
}

void Cookware::print() {
    std::cout << "Inventory id: " << this->inventoryId << "\n";
}

bool Cookware::operator <(const Cookware &b) {
    return this->inventoryId < b.inventoryId;
}

bool Cookware::operator >(const Cookware &b) {
    return this->inventoryId > b.inventoryId;
}

bool Cookware::operator <=(const Cookware &b) {
    return !(*this > b);
}

bool Cookware::operator >=(const Cookware &b) {
    return !(*this < b);
}

bool Cookware::operator ==(const Cookware &b) {
    return this->inventoryId == b.inventoryId;
}

bool Cookware::operator !=(const Cookware &b) {
    return !(*this == b);
}

Cookware::operator unsigned long long() {
    return this->inventoryId;
}

Cookware::operator int() {
    return this->inventoryId;
}

std::ostream& Cookware::print(std::ostream& os) {
    os << "Inventory id: " << this->inventoryId << "\n---------------\n";
    return os;
}

std::ostream& operator<<(std::ostream& os, Cookware& c) {
    return c.print(os);
}
