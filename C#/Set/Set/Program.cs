﻿using System;
using System.IO;
using System.Runtime.CompilerServices;

class OutOfSetRangeException : Exception
{
    public OutOfSetRangeException()
    {
    }

    public OutOfSetRangeException(string message) : base(message)
    {
    }

    public OutOfSetRangeException(string message, Exception inner) : base(message, inner)
    {
    }
}

abstract class Set
{
    protected int maxSize;
    public abstract void Append(int item);
    public abstract void Remove(int item);
    public abstract bool Elem(int item);

    public void FillSet(string items)
    {
        foreach (var item in items.Split())
        {
            try
            {
                this.Append(int.Parse(item));
            }
            catch (OutOfSetRangeException e)
            {
                Console.WriteLine(e.Message);

                return;
                // throw;
            }
        }
    }

    public void FillSet(int[] items)
    {
        foreach (var item in items)
        {
            this.Append(item);
        }
    }

    public void Show()
    {
        for (int i = 1; i <= this.maxSize; i++)
        {
            if (Elem(i))
            {
                Console.Write(i.ToString() + ' ');
            }
        }

        Console.WriteLine();
    }
}

class SimpleSet : Set
{
    private bool[] items;

    public SimpleSet(int limit)
    {
        this.maxSize = limit;
        this.items = new bool[limit];
    }

    public override void Append(int item)
    {
        if (item > this.maxSize || item <= 0)
        {
            throw new OutOfSetRangeException(
                "Ошибка! Аргумент вне области допустимых значений [1:"
                + this.maxSize.ToString() + "]");
        }
        else
        {
            this.items[item - 1] = true;
        }
    }

    public override void Remove(int item)
    {
        if (item > this.maxSize || item <= 0)
        {
            throw new OutOfSetRangeException(
                "Ошибка! Аргумент вне области допустимых значений [1:"
                + this.maxSize.ToString() + "]");
        }
        else
        {
            this.items[item - 1] = false;
        }
    }

    public override bool Elem(int item)
    {
        if (item > this.maxSize || item <= 0)
        {
            throw new OutOfSetRangeException(
                "Ошибка! Аргумент вне области допустимых значений [1:"
                + this.maxSize.ToString() + "]");
        }
        else
        {
            return this.items[item - 1];
        }
    }

    public static SimpleSet operator +(SimpleSet set1, SimpleSet set2)
    {
        SimpleSet minSet, maxSet;

        if (set1.maxSize > set2.maxSize)
        {
            minSet = set2;
            maxSet = set1;
        }
        else
        {
            minSet = set1;
            maxSet = set2;
        }

        SimpleSet resultSet = new SimpleSet(maxSet.maxSize);

        int i = 1;
        for (; i <= minSet.maxSize; i++)
        {
            if (minSet.Elem(i) || maxSet.Elem(i))
            {
                resultSet.Append(i);
            }
        }

        for (; i <= maxSet.maxSize; i++)
        {
            if (maxSet.Elem(i))
            {
                resultSet.Append(i);
            }
        }

        return resultSet;
    }

    public static SimpleSet operator *(SimpleSet set1, SimpleSet set2)
    {
        SimpleSet minSet, maxSet;

        if (set1.maxSize > set2.maxSize)
        {
            minSet = set2;
            maxSet = set1;
        }
        else
        {
            minSet = set1;
            maxSet = set2;
        }

        SimpleSet resultSet = new SimpleSet(minSet.maxSize);

        int i = 1;
        for (; i <= minSet.maxSize; i++)
        {
            if (minSet.Elem(i) && maxSet.Elem(i))
            {
                resultSet.Append(i);
            }
        }

        return resultSet;
    }
}

class BitSet : Set
{
    private int[] items;

    public BitSet(int limit)
    {
        if (limit <= 0)
            throw new OutOfSetRangeException("Число элементов не может быть <= 0");

        this.maxSize = limit;
        this.items = new int[(limit - 1) / (8 * sizeof(int)) + 1];
    }

    public override void Append(int item)
    {
        if (item > maxSize || item <= 0)
            throw new OutOfSetRangeException("Элемент "
                                             + item.ToString()
                                             + " вне допустимой области [1:"
                                             + this.maxSize.ToString()
                                             + "]");
        int index = item / (8 * sizeof(int));
        this.items[index] |= 1 << (item - 1);
    }

    public override void Remove(int item)
    {
        if (item > maxSize || item <= 0)
            throw new OutOfSetRangeException("Элемент "
                                             + item.ToString()
                                             + " вне допустимой области [1:"
                                             + this.maxSize.ToString()
                                             + "]");
        int index = item / (8 * sizeof(int));
        this.items[index] &= ~(1 << (item - 1));
    }

    public override bool Elem(int item)
    {
        if (item > maxSize || item <= 0)
            return false;

        int index = item / (8 * sizeof(int));
        return (this.items[index] & (1 << (item - 1))) != 0;
    }

    public static BitSet operator +(BitSet set1, BitSet set2)
    {
        BitSet minSet, maxSet;

        if (set1.maxSize > set2.maxSize)
        {
            minSet = set2;
            maxSet = set1;
        }
        else
        {
            minSet = set1;
            maxSet = set2;
        }

        var resultSet = new BitSet((int) maxSet.maxSize);

        int i = 0;
        for (; i < minSet.items.Length; i++)
        {
            resultSet.items[i] = minSet.items[i] | maxSet.items[i];
        }

        for (; i < maxSet.items.Length; i++)
        {
            resultSet.items[i] = maxSet.items[i];
        }

        return resultSet;
    }

    public static BitSet operator *(BitSet set1, BitSet set2)
    {
        BitSet minSet, maxSet;

        if (set1.maxSize > set2.maxSize)
        {
            minSet = set2;
            maxSet = set1;
        }
        else
        {
            minSet = set1;
            maxSet = set2;
        }

        var resultSet = new BitSet((int) minSet.maxSize);

        for (int i = 0; i < minSet.items.Length; i++)
        {
            resultSet.items[i] = minSet.items[i] & maxSet.items[i];
        }

        return resultSet;
    }
}

class MultiSet : Set
{
    private int[] items;

    public MultiSet(int limit)
    {
        if (limit <= 0)
            throw new OutOfSetRangeException("Число элементов в множестве не может быть <= 0");

        this.maxSize = limit;
        this.items = new int[limit];
    }

    public override void Append(int item)
    {
        if (item <= 0 || item > this.maxSize)
            throw new OutOfSetRangeException("Элемент "
                                             + item.ToString()
                                             + " вне допустимой области [1:" +
                                             +this.maxSize
                                             + "]");

        this.items[item - 1]++;
    }

    public override void Remove(int item)
    {
        if (item <= 0 || item > this.maxSize)
            throw new OutOfSetRangeException("Элемент "
                                             + item.ToString()
                                             + " вне допустимой области [1:" +
                                             +this.maxSize
                                             + "]");
        if (this.items[item - 1] > 0)
            this.items[item - 1]--;
    }

    public override bool Elem(int item)
    {
        if (item <= 0 || item > this.maxSize)
            throw new OutOfSetRangeException("Элемент "
                                             + item.ToString()
                                             + " вне допустимой области [1:" +
                                             +this.maxSize
                                             + "]");

        return this.items[item - 1] > 0;
    }

    public static MultiSet operator +(MultiSet set1, MultiSet set2)
    {
        MultiSet minSet, maxSet;

        if (set1.maxSize > set2.maxSize)
        {
            minSet = set2;
            maxSet = set1;
        }
        else
        {
            minSet = set1;
            maxSet = set2;
        }

        var resultSet = new MultiSet((int) maxSet.maxSize);

        int i = 0;
        for (; i < minSet.maxSize; i++)
        {
            resultSet.items[i] = minSet.items[i] + maxSet.items[i];
        }

        for (; i < maxSet.maxSize; i++)
        {
            resultSet.items[i] = maxSet.items[i];
        }

        return resultSet;
    }

    public static MultiSet operator *(MultiSet set1, MultiSet set2)
    {
        MultiSet minSet, maxSet;

        if (set1.maxSize > set2.maxSize)
        {
            minSet = set2;
            maxSet = set1;
        }
        else
        {
            minSet = set1;
            maxSet = set2;
        }

        var resultSet = new MultiSet((int) minSet.maxSize);

        int i = 0;
        for (; i < minSet.maxSize; i++)
        {
            resultSet.items[i] = minSet.items[i] + maxSet.items[i];
        }

        return resultSet;
    }
}

namespace SetApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string userInput;
            int limit;
            Set set;

            Console.WriteLine("Добро пожаловать!");
            Console.WriteLine("1 или 2 множества?");

            userInput = Console.ReadLine();
            if (userInput.Trim() == "2")
            {
                Console.WriteLine("Выберите представление: simple, bit или multiset:");
                userInput = Console.ReadLine();

                Console.WriteLine("Введите максимальное число первого множества:");
                int limit1 = Int32.Parse(Console.ReadLine());

                Console.WriteLine("Введите максимальное число второго множества:");
                int limit2 = Int32.Parse(Console.ReadLine());

                Console.WriteLine("Объединение или пересечение?");
                string op = Console.ReadLine();

                if (userInput == "simple")
                {
                    SimpleSet set1 = new SimpleSet(limit1);
                    SimpleSet set2 = new SimpleSet(limit2);

                    set1.FillSet(Console.ReadLine());
                    set2.FillSet(Console.ReadLine());

                    if (op.ToLower() == "объединение")
                        (set1 + set2).Show();
                    else
                        (set1 * set2).Show();
                }
                else if (userInput == "bit")
                {
                    BitSet set1 = new BitSet(limit1);
                    BitSet set2 = new BitSet(limit2);

                    set1.FillSet(Console.ReadLine());
                    set2.FillSet(Console.ReadLine());
                    
                    if (op.ToLower() == "объединение")
                        (set1 + set2).Show();
                    else
                        (set1 * set2).Show();
                }
                else if (userInput == "multiset")
                {
                    MultiSet set1 = new MultiSet(limit1);
                    MultiSet set2 = new MultiSet(limit2);

                    set1.FillSet(Console.ReadLine());
                    set2.FillSet(Console.ReadLine());

                    if (op.ToLower() == "объединение")
                        (set1 + set2).Show();
                    else
                        (set1 * set2).Show();
                }
                else
                {
                    Console.WriteLine("Некорректный ввод! Попробуйте ещё раз");
                    return;
                }
            }

            Console.WriteLine("Выберите представление: simple, bit или multiset:");

            while (true)
            {
                userInput = Console.ReadLine();
                if (userInput == null)
                    return;

                Console.WriteLine("Введите максимальное число:");
                limit = Int32.Parse(Console.ReadLine());

                userInput = userInput.Trim().ToLower();

                if (userInput == "simple")
                {
                    set = new SimpleSet(limit);
                    break;
                }
                else if (userInput == "bit")
                {
                    set = new BitSet(limit);
                    break;
                }
                else if (userInput == "multiset")
                {
                    set = new MultiSet(limit);
                    break;
                }
                else
                    Console.WriteLine("Некорректный ввод! Попробуйте ещё раз");
            }

            while (true)
            {
                Console.WriteLine("Вносить значения с [к]лавиатуры или из [ф]айла?");
                userInput = Console.ReadLine();
                if (userInput == null)
                    return;

                userInput = userInput.Trim().ToLower();

                if (userInput == "к")
                {
                    Console.WriteLine("Введите значения, разделённые пробелом:");
                    userInput = Console.ReadLine();

                    try
                    {
                        set.FillSet(userInput);
                    }
                    catch (OutOfSetRangeException e)
                    {
                        Console.WriteLine(e.Message);
                    }

                    break;
                }
                else if (userInput == "ф")
                {
                    Console.WriteLine("Введите путь до файла:");
                    userInput = Console.ReadLine();

                    // TODO: File input
                    try
                    {
                        using (StreamReader sr = new StreamReader(userInput))
                        {
                            string line;
                            while ((line = sr.ReadLine()) != null)
                            {
                                int[] items = Array.ConvertAll(line.Split(), int.Parse);
                                set.FillSet(items);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        continue;
                    }
                }
                else
                {
                    Console.WriteLine("Некорректный ввод!");
                }
            }

            while (true)
            {
                set.Show();
                Console.WriteLine("Доступные операции: добавить, удалить, проверить, выход");
                userInput = Console.ReadLine();
                if (userInput == null)
                    break;

                userInput = userInput.Trim().ToLower();
                if (userInput == "выход")
                    break;

                if (userInput == "добавить" || userInput == "удалить" || userInput == "проверить")
                {
                    Console.WriteLine("Введите число:");
                    int item;

                    try
                    {
                        item = Int32.Parse(Console.ReadLine());
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        continue;
                    }

                    if (userInput == "добавить")
                    {
                        try
                        {
                            set.Append(item);
                        }
                        catch (OutOfSetRangeException e)
                        {
                            Console.WriteLine(e.Message);
                            continue;
                        }
                    }
                    else if (userInput == "удалить")
                    {
                        try
                        {
                            set.Remove(item);
                        }
                        catch (OutOfSetRangeException e)
                        {
                            Console.WriteLine(e.Message);
                            continue;
                        }
                    }
                    else
                    {
                        try
                        {
                            Console.WriteLine(set.Elem(item)
                                ? ("Элемент " + item.ToString() + " во множестве!")
                                : ("Элемент " + item.ToString() + " отсутствует!"));
                        }
                        catch (OutOfSetRangeException e)
                        {
                            Console.WriteLine(e.Message);
                            continue;
                        }
                    }
                }
            }
        }
    }
}
