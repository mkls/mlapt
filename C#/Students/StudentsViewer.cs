using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Gtk;
using UI = Gtk.Builder.ObjectAttribute;
using System.IO;
using System.Xml;

namespace Students
{
    class StudentsViewer : ApplicationWindow
    {
        enum Columns { name, surname, faculty, master, diploma };

        // [UI] private ListStore studentsList = null;
        [UI] private InfoBar infoBar = null;
        [UI] private Label infoBarLabel = null;

        [UI] private HeaderBar headerBar = null;

        [UI] private TreeSelection treeSelection = null;
        [UI] private TreeView treeView = null;

        [UI] private Button appendButton = null;
        [UI] private Button removeButton = null;
        [UI] private Button nextButton = null;
        [UI] private Button prevButton = null;
        [UI] private Button saveButton = null;

        [UI] private ToggleButton searchButton = null;

        [UI] private ComboBoxText filterComboBox = null;

        [UI] private Entry filterEntry = null;

        [UI] private FileChooserButton openFileButton = null;

        [UI] private TreeViewColumn nameColumn = null;
        [UI] private TreeViewColumn surnameColumn = null;
        [UI] private TreeViewColumn facultyColumn = null;
        [UI] private TreeViewColumn masterColumn = null;
        [UI] private TreeViewColumn diplomaColumn = null;

        [UI] private CellRendererText cellRendererName = null;
        [UI] private CellRendererText cellRendererSurname = null;
        [UI] private CellRendererText cellRendererFaculty = null;
        [UI] private CellRendererToggle cellRendererMaster = null;
        [UI] private CellRendererText cellRendererMasterDiploma = null;

        [UI] private Box filterBox = null;

        private ListStore studentsList = null;
        private List<TreeIter> searchResults;
        private int searchSelectedIndex = 0;

        public StudentsViewer() : this(new Builder("StudentViewer.glade")) { }

        private StudentsViewer(Builder builder) : base(builder.GetObject("appWindow").Handle)
        {
            builder.Autoconnect(this);
            DeleteEvent += Window_DeleteEvent;

            studentsList = new ListStore(typeof(Student));

            this.nameColumn.SetCellDataFunc(cellRendererName, RenderStudentName);
            this.surnameColumn.SetCellDataFunc(cellRendererSurname, RenderStudentSurame);
            this.facultyColumn.SetCellDataFunc(cellRendererFaculty, RenderStudentFaculty);
            this.masterColumn.SetCellDataFunc(cellRendererMaster, RenderStudentMaster);
            this.diplomaColumn.SetCellDataFunc(cellRendererMasterDiploma, RenderStudentDiploma);

            this.treeView.Model = this.studentsList;
        }

        private void Window_DeleteEvent(object sender, DeleteEventArgs a)
        {
            Application.Quit();
        }

        private void onNewFileButtonClicked(object sender, EventArgs a)
        {
            this.studentsList.Clear();
            this.appendButton.SetProperty("sensitive", new GLib.Value(true));
            this.removeButton.SetProperty("sensitive", new GLib.Value(false));
            this.saveButton.SetProperty("sensitive", new GLib.Value(true));

            this.headerBar.Subtitle = "Создан новый файл";
        }

        private void onSaveButtonClicked(object sender, EventArgs a)
        {
            var saveDialog = new FileChooserDialog("Выберите файл для сохранения",
                                                   this,
                                                   FileChooserAction.Save,
                                                   "Отмена", ResponseType.Cancel,
                                                   "Сохранить", ResponseType.Accept);
            if (saveDialog.Run() == (int)ResponseType.Accept)
            {

                var filePath = saveDialog.Filename;
                this.headerBar.Subtitle = filePath;

                this.openFileButton.SetFilename(filePath);
                this.openFileButton.SelectFilename(filePath);

                List<Student> students = new List<Student>();

                foreach (object[] student in studentsList)
                {
                    students.Add((Student)student[0]);
                }

                XmlWriterSettings settings = new XmlWriterSettings() { Indent = true };
                using (XmlWriter writer = XmlWriter.Create(filePath, settings))
                {
                    DataContractSerializer serializer = new DataContractSerializer(typeof(List<Student>));
                    serializer.WriteObject(writer, students);
                }
            }

            saveDialog.Dispose();
        }

        private void onAppendButtonClicked(object sender, EventArgs a)
        {
            var iter = this.studentsList.AppendValues(new Student("Test", "Surname", "Test"));
            this.treeSelection.SelectIter(iter);

            var path = this.studentsList.GetPath(iter);
            var column = this.treeView.GetColumn(0);

            this.removeButton.SetProperty("sensitive", new GLib.Value(true));


            this.treeView.ActivateRow(path, column);
            this.treeView.SetCursorOnCell(path, column, this.cellRendererName, true);
            this.treeView.GrabFocus();
        }

        private void onRemoveButtonClicked(object sender, EventArgs a)
        {
            TreeIter selectedRow;
            this.treeSelection.GetSelected(out selectedRow);
            this.studentsList.Remove(ref selectedRow);

            if (this.studentsList.IterNChildren() == 0)
            {
                this.removeButton.SetProperty("sensitive", new GLib.Value(false));
                this.filterBox.SetProperty("sensitive", new GLib.Value(false));
                this.searchButton.SetProperty("sensitive", new GLib.Value(false));
            }
        }

        private void onCellRendererMasterToggled(object sender, ToggledArgs a)
        {
            TreeIter iter;
            this.studentsList.GetIter(out iter, new TreePath(a.Path));

            var student = (Student)this.studentsList.GetValue(iter, 0);

            if (!(student is Master)) {
                this.studentsList.SetValue(iter, 0, Master.FromStudent(student, "Введите данные о дипломе"));
            } else {
                this.studentsList.SetValue(iter, 0, new Student(student.name, student.surname, student.faculty));
            }
        }

        private void onCellRendererNameEdited(object sender, EditedArgs a)
        {
            TreeIter iter;
            this.studentsList.GetIter(out iter, new TreePath(a.Path));

            Student student = (Student)studentsList.GetValue(iter, 0);
            student.name = a.NewText;

            onFilterEntryChanged(this.filterEntry, a);
        }

        private void onCellRendererSurnameEdited(object sender, EditedArgs a)
        {
            TreeIter iter;
            this.studentsList.GetIter(out iter, new TreePath(a.Path));

            Student student = (Student)studentsList.GetValue(iter, 0);
            student.surname = a.NewText;

            onFilterEntryChanged(this.filterEntry, a);
        }

        private void onCellRendererFacultyEdited(object sender, EditedArgs a)
        {
            TreeIter iter;
            this.studentsList.GetIter(out iter, new TreePath(a.Path));
            Student s = (Student)this.studentsList.GetValue(iter, 0);
            s.faculty = a.NewText;

            onFilterEntryChanged(this.filterEntry, a);
        }

        private void onFileSet(object sender, EventArgs a)
        {
            var filename = ((FileChooserButton)sender).Filename;
            using (FileStream sr = new FileStream(filename, FileMode.Open))
            {
                DataContractSerializer serializer = new DataContractSerializer(typeof(List<Student>));
                List<Student> students;
                try
                {
                    students = (List<Student>)serializer.ReadObject(sr);
                }
                catch (Exception)
                {
                    this.infoBar.SetProperty("revealed", new GLib.Value(true));
                    this.infoBar.SetProperty("message-type", new GLib.Value(MessageType.Error));
                    this.infoBarLabel.Text = $"Ошибка открытия файла {filename}. Возможно, ошибка в структуре документа?";

                    return;
                }

                this.studentsList.Clear();
                foreach (Student s in students)
                {
                    studentsList.AppendValues(s);
                }

                this.infoBar.SetProperty("revealed", new GLib.Value(true));
                this.infoBar.SetProperty("message-type", new GLib.Value(MessageType.Info));
                this.infoBarLabel.Text = "Открыт " + filename;
                this.headerBar.SetProperty("subtitle", new GLib.Value(filename));
                this.saveButton.SetProperty("sensitive", new GLib.Value(true)); 
            }
            if (studentsList.IterNChildren() > 0) {

                TreeIter iter;
                this.studentsList.GetIterFirst(out iter);

                this.searchButton.SetProperty("sensitive", new GLib.Value(true));
                this.removeButton.SetProperty("sensitive", new GLib.Value(true));
                this.treeSelection.SelectIter(iter);
            }
            this.appendButton.SetProperty("sensitive", new GLib.Value(true));
        }

        private void onInfoBarResponse(object sender, RespondArgs a)
        {
            this.infoBar.SetProperty("revealed", new GLib.Value(false));
            this.treeView.GrabDefault();
        }


        private void onCellRendererMasterDiplomaEdited(object sender, EditedArgs a)
        {
            TreeIter iter;
            this.studentsList.GetIter(out iter, new TreePath(a.Path));

            Master s = (Master)this.studentsList.GetValue(iter, 0);
            s.diplomaNumber = a.NewText;
        }

        private void RenderStudentName(TreeViewColumn column, CellRenderer cell, ITreeModel model, TreeIter iter)
        {
            Student s = (Student)this.studentsList.GetValue(iter, 0);
            (cell as CellRendererText).Text = s.name;
        }

        private void RenderStudentSurame(TreeViewColumn column, CellRenderer cell, ITreeModel model, TreeIter iter)
        {
            Student s = (Student)this.studentsList.GetValue(iter, 0);
            (cell as CellRendererText).Text = s.surname;
        }

        private void RenderStudentFaculty(TreeViewColumn column, CellRenderer cell, ITreeModel model, TreeIter iter)
        {
            Student s = (Student)this.studentsList.GetValue(iter, 0);
            (cell as CellRendererText).Text = s.faculty;
        }

        private void RenderStudentMaster(TreeViewColumn column, CellRenderer cell, ITreeModel model, TreeIter iter)
        {
            Student s = (Student)this.studentsList.GetValue(iter, 0);
            bool res = true;

            if (!(s is Master))
            {
                res = false;
            }

            (cell as CellRendererToggle).Active = res;
        }

        private void RenderStudentDiploma(TreeViewColumn column, CellRenderer cell, ITreeModel model, TreeIter iter)
        {
            Student s = (Student)this.studentsList.GetValue(iter, 0);

            string res = "";
            bool editable = false;

            if (s is Master)
            {
                res = (s as Master).diplomaNumber;
                editable = true;
            }

            (cell as CellRendererText).Editable = editable;
            (cell as CellRendererText).Text = res;
        }

        private void onPrevButtonClicked(object sender, EventArgs args)
        {
            this.searchSelectedIndex -= 1;
            this.treeSelection.SelectIter(searchResults[searchSelectedIndex]);
            this.nextButton.SetProperty("sensitive", new GLib.Value(true));

            this.headerBar.Title = $"Результат {this.searchSelectedIndex + 1}/{this.searchResults.Count()}";

            if (this.searchSelectedIndex == 0) {
                this.prevButton.SetProperty("sensitive", new GLib.Value(false));

                return;
            }
        }

        private void onNextButtonClicked(object sender, EventArgs args)
        {
            this.searchSelectedIndex += 1;
            this.treeSelection.SelectIter(searchResults[searchSelectedIndex]);
            this.prevButton.SetProperty("sensitive", new GLib.Value(true));

            this.headerBar.Title = $"Результат {this.searchSelectedIndex + 1}/{this.searchResults.Count()}";

            if (this.searchSelectedIndex == this.searchResults.Count - 1)
            {
                this.nextButton.SetProperty("sensitive", new GLib.Value(false));

                return;
            }
        }

        private void onFilterEntryChanged(object sender, EventArgs args)
        {
            if (filterEntry.Text == "")
            {
                this.headerBar.Title = "Просмотр студентов";
                return;
            } 

            List<TreeIter> students = new List<TreeIter>();

            TreeIter iter;
            if (!this.studentsList.GetIterFirst(out iter))
                return;

            for (int i = 0; i < this.studentsList.IterNChildren(); i++)
            {
                students.Add(iter);
                this.studentsList.IterNext(ref iter);
            }

            switch (this.filterComboBox.Active)
            {
                case (int)Columns.name:
                    searchResults = students.Where(
                        istudent => ((Student)studentsList.GetValue(istudent, 0)).name == filterEntry.Text).ToList();
                    break;
                case (int)Columns.surname:
                    searchResults = students.Where(
                        istudent => ((Student)studentsList.GetValue(istudent, 0)).surname == filterEntry.Text).ToList();
                    break;
                default:
                    searchResults = students.Where(
                        istudent => ((Student)studentsList.GetValue(istudent, 0)).faculty == filterEntry.Text).ToList();
                    break;
            }

            TreeIter first;

            if (searchResults.Count() == 0)
            {
                this.headerBar.Title = "Ничего не найдено ☹️";
                this.nextButton.SetProperty("sensitive", new GLib.Value(false));
                this.prevButton.SetProperty("sensitive", new GLib.Value(false));
                return;
            }

            first = searchResults.First();

            this.searchSelectedIndex = 0;
            this.treeSelection.SelectIter(first);

            if (searchResults.Count() > 1) {
                this.nextButton.SetProperty("sensitive", new GLib.Value(true));
                this.prevButton.SetProperty("sensitive", new GLib.Value(false));
            }

            this.headerBar.Title = $"Результат 1/{searchResults.Count()}";
        }

        private void onFilterComboBoxChanged(object sender, EventArgs args)
        {
            this.onFilterEntryChanged(this.filterEntry, args);
        }

        private void onSelectionChanged(object sender, EventArgs args)
        {
            TreeIter iter;
            
            if (!this.studentsList.GetIterFirst(out iter))
                return;

            this.treeSelection.GetSelected(out iter);
            this.treeView.ScrollToCell(this.studentsList.GetPath(iter), null, false, 0.5f, 0.5f);
        }

        private void onSearchButtonToggled(object sender, EventArgs args)
        {
            if (!this.searchButton.Active)
            {
                this.filterBox.SetProperty("visible", new GLib.Value(false));
                this.appendButton.SetProperty("sensitive", new GLib.Value(true));
                this.removeButton.SetProperty("sensitive", new GLib.Value(true));
                this.headerBar.Title = "Просмотр студентов";
            } else
            {
                this.filterEntry.Text = "";
                this.appendButton.SetProperty("sensitive", new GLib.Value(false));
                this.removeButton.SetProperty("sensitive", new GLib.Value(false));
                this.filterBox.SetProperty("visible", new GLib.Value(true));
            }
        }
    }

}