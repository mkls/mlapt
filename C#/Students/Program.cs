using System;
using Gtk;

namespace Students
{
    class Program
    {
        [STAThread]
        public static void Main(string[] args)
        {
            Application.Init();

            var app = new Application("org.Students.Students", GLib.ApplicationFlags.None);
            app.Register(GLib.Cancellable.Current);

            var win = new StudentsViewer();
            app.AddWindow(win);

            win.Show();
            Application.Run();
        }
    }
}
