using System.Runtime.Serialization;

namespace Students
{
    [DataContract]
    [KnownType(typeof(Master))]
    public class Student
    {
        [DataMember] public string name { get; set; }
        [DataMember] public string surname { get; set; }
        [DataMember] public string faculty { get; set; }

        public Student(string name, string surname, string faculty)
        {
            this.name = name;
            this.surname = surname;
            this.faculty = faculty;
        }
    }

    [DataContract]
    public class Master : Student
    {
        [DataMember] public string diplomaNumber { get; set; }

        public Master(string name, string surname, string faculty, string diplomaNumber) : base(name, surname, faculty)
        {
            this.diplomaNumber = diplomaNumber;
        }

        public static Master FromStudent(Student s, string diploma = "-")
        {
            return new Master(s.name, s.surname, s.faculty, diploma);
        }
    }
}
