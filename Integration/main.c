#include "HsFFI.h"
#include "knapsack_stub.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    hs_init(&argc, &argv);
    
    int costs[] = {6, 10, 3, 25, 5};
    int weights[] = {3, 6, 3, 5, 4};
    int limit = 16;

    int *res = NULL;

    int elements_found = knapsack_c(costs, weights, 5, &res, limit);

    printf("Найдено элементов: %i\n", elements_found);

    for (int i = 0; i < elements_found; i++) {
        printf("%i ", res[i]);
    }

    hs_exit();

    return 0;
}
