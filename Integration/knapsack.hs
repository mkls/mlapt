{-# LINE 1 "knapsack.hsc" #-}
{-# LANGUAGE CPP #-}
{-# LANGUAGE ForeignFunctionInterface #-}

module Knapsack where

import Data.List
import Foreign
import Foreign.C.Types
import Foreign.Ptr
import Foreign.Marshal.Array
import Foreign.Marshal.Alloc

foreign export ccall knapsack_c :: Ptr CInt -> Ptr CInt -> CInt -> Ptr (Ptr CInt) -> CInt -> IO CInt
foreign export ccall freeKnapsack :: Ptr CInt -> IO()

-- Knapsack solver gets 5 args from main program:
--            costs, weights, num of inputs, filtered ids, limit
--     returns number of picked items
knapsack_c :: Ptr CInt -> Ptr CInt -> CInt -> Ptr (Ptr CInt) -> CInt -> IO CInt
knapsack_c ptr_costs ptr_weights n res_ptr limit = do
    let m = fromIntegral n
    -- Convert input arrays to Haskell List to pass into knapsack solver
    -- ids <- peekArray (fromIntegral n) ptr_ids 
    costs <- peekArray m ptr_costs 
    weights <- peekArray m ptr_weights 

    let items = zip3 (take m [1..]) costs weights
    let res = map (\(x,_,_) -> x) $ knapsack items limit
    let len = fromIntegral (length res)

    -- Assign new_ptr value of pointer directing to resulting id array
    -- and write the value into res_ptr
    --deref <- peek res_ptr
    new_ptr <- newArray res
    poke res_ptr new_ptr
    -- Return number of resulting elements
    return len

freeKnapsack::Ptr (CInt) -> IO()
freeKnapsack knapsack_ptr = do
    free knapsack_ptr

    return ()

knapsack [] _ = []
knapsack items limit =
    findBest (subsequences items) limit 0 [] where
        findBest [] _ _ res = res
        findBest (h:t) limit acc res
            | costs > acc && weights <= limit = findBest t limit costs h
            | otherwise = findBest t limit acc res where
                costs = foldl' (+) 0 (map (\(_, x, _) -> x) h)
                weights = foldl' (+) 0 (map (\(_, _, x) -> x) h)
