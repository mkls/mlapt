#!/usr/bin/python

import gi
gi.require_version('Gtk', '3.0')

from gi.repository import Gtk
from ctypes import *

class Window:
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file('ui.glade')

        self.__window = builder.get_object('app_window')
        self.__file_chooser = builder.get_object('file_chooser_button')
        self.__solve_button = builder.get_object('solve_button')
        self.__costs_entry = builder.get_object('costs_entry')
        self.__weights_entry = builder.get_object('weights_entry')
        self.__costs_res_entry = builder.get_object('costs_res_entry')
        self.__weights_res_entry = builder.get_object('weights_res_entry')
        self.__limit_entry = builder.get_object('limit_entry')
        self.__dialog = builder.get_object('message_dialog')

        self.__not_enough_inputs = 'Not enough inputs given. Please re-check all the entries.'
        self.__invalid_lib = 'Chosen library file does not contain required haskell function'
        self.__lib = None

        handlers = {
            'on_destroy': Gtk.main_quit,
            'on_file_set': self.__on_file_set,
            'on_solve_button_clicked': self.__on_solve_button_clicked
        }

        builder.connect_signals(handlers)

        self.__window.show_all()

    def __launch_dialog(self, text: str):
        main_text = self.__dialog.get_message_area().get_children()[0]
        main_text.set_text(text)

        self.__dialog.run()
        self.__dialog.hide()

    def __on_file_set(self, *args):
        filename = self.__file_chooser.get_filename()

        if filename is not None:
            try:
                self.__lib = CDLL(filename)
                self.__solver_func = getattr(self.__lib, 'knapsack_c')
                self.__free_func = getattr(self.__lib, 'freeKnapsack')
                self.__solve_button.set_sensitive(True)
            except OSError:
                self.__solve_button.set_sensitive(False)
                self.__launch_dialog(self.__invalid_lib)

    def __on_solve_button_clicked(self, *args):
        costs = []
        weights = []

        try:
            costs = [int(i) for i in self.__costs_entry.get_text().split()]
            weights = [int(i) for i in self.__weights_entry.get_text().split()]
        # TODO: it does not work, but it does not crash, lul
        except TypeError:
            self.__launch_dialog('Please provide valid inputs. Only integer numbers are allowed.')

        if len(costs) != len(weights) or self.__limit_entry.get_text() == '':
            self.__launch_dialog(self.__not_enough_inputs)
        elif len(self.__limit_entry.get_text().split()) > 1:
            self.__launch_dialog('Provide only 1 number for knapsack limit')
        else:
            result_array = pointer(c_int())
            limit = c_int(int(self.__limit_entry.get_text()))
            c_costs = (c_int * len(costs))(*costs)
            c_weights = (c_int * len(weights))(*weights)
            returned_elements = self.__solver_func(c_costs, c_weights, len(costs), pointer(result_array), limit)

            costs_str = ''
            weights_str = ''

            for i in range(returned_elements):
                costs_str += str(costs[result_array[i] - 1]) + ' '
                weights_str += str(weights[result_array[i] - 1]) + ' '

            self.__costs_res_entry.set_text(costs_str)
            self.__weights_res_entry.set_text(weights_str)

            self.__free_func(result_array)


window = Window()
Gtk.main()

