#include <HsFFI.h>

extern void __stginit_Knapsack (void);

static void library_init (void) __attribute__ ((constructor));
static void
library_init (void)
{
  /* This seems to be a no-op, but it makes the GHCRTS envvar work. */
  static char *argv[] = { "libknapsack.so", 0 }, **argv_ = argv;
  static int argc = 1;

  hs_init (&argc, &argv_);
  //hs_add_root (__stginit_Knapsack);
}

static void library_exit (void) __attribute__ ((destructor));
static void
library_exit (void)
{
  hs_exit ();
}
